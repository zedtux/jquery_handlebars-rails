# jquery_handlebars-rails

jquery-handlebars is a jQuery plugin to render Handlebars.js templates into elements.

The project's page is at http://71104.github.io/jquery-handlebars/.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'jquery_handlebars-rails'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jquery_handlebars-rails

## Usage

Add the following directive to your Javascript manifest file (application.js):

    //= require jquery-handlebars

## Contributing

1. Fork it ( https://github.com/[my-github-username]/jquery_handlebars-rails/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
