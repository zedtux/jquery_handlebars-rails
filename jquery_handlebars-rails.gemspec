# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'jquery_handlebars/rails/version'

Gem::Specification.new do |spec|
  spec.name          = 'jquery_handlebars-rails'
  spec.version       = JqueryHandlebars::Rails::VERSION
  spec.authors       = ['zedtux']
  spec.email         = ['zedtux@zedroot.org']
  spec.summary       = %q{The jquery-handlebars JavaScript library ready to
                          play with Rails.}
  spec.description   = %q{jquery-handlebars is a jQuery plugin to render
                          Handlebars.js templates into elements and this gem
                          integrates it to your Rails application.}
  spec.homepage      = 'https://github.com/zedtux/jquery_handlebars-rails'
  spec.license       = 'MIT'

  spec.files         = Dir["{lib,vendor}/**/*"] | ['LICENSE', 'README.md']
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake', '~> 10.0'

  spec.add_dependency 'railties', '~> 4.1'
end
